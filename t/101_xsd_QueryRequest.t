use warnings;
use strict;

use Test::More qw(no_plan);
use XML::LibXML;

require 't/lib/Common.pm';

my $xsd = "./xsd/QueryRequest.xsd";
my $parser = XML::LibXML->new();

## Create a validator object
my $xs = create_xs($xsd);

#### XML tests
my $fn;                        # filename
my $test;                      # test description (english)
my $failure = 1; my $diag = 1; # true values

$test = "minimal valid request (AndStrings)";
$fn   = "./t/sample_xml/QueryRequest/v_Minimal_AndStrings.xml";
xml_test($parser, $xs, $fn, $test);

$test = "minimal valid request (OrStrings)";
$fn   = "./t/sample_xml/QueryRequest/v_Minimal_OrStrings.xml";
xml_test($parser, $xs, $fn, $test);

$test = "invalid request (no search strings)";
$fn   = "./t/sample_xml/QueryRequest/i_NoSearchStrings.xml";
xml_test($parser, $xs, $fn, $test, $failure);

$test = "invalid request (no timestamp)";
$fn   = "./t/sample_xml/QueryRequest/i_NoClientTimestamp.xml";
xml_test($parser, $xs, $fn, $test, $failure);

