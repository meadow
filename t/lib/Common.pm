use strict;
use warnings;

use Test::More;
use XML::LibXML;

sub create_xs {
    my $xsd = shift;

    my $xs; eval {$xs = XML::LibXML::Schema->new( location => $xsd )};
    ok(!$@, "creating validator object (xsd valid)") or diag($@);

    return $xs;
}

sub xml_test {
    (my $parser, my $xs, my $fn, my $test, my $expect_failure, my $diag_error) = @_;

    # Open the file.
    my $fh;
    eval {open $fh, $fn};
    ok(!$@, "opening $test");

    # Parse the XML.
    my $xml;
    eval {$xml = $parser->parse_fh($fh)};
    ok(!$@, "parsing $test, valid xml");
    diag($@) if $diag_error;

    # Validate against the XSD.
    eval {$xs->validate($xml)};
    if ($expect_failure) {
        ok($@, "validating $test (failure expected)");
    } 
    else {
        ok(!$@, "validating $test");
    }
    
    diag($@) if $diag_error;
}

1;
