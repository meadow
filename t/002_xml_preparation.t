use warnings;
use strict;

use Test::More qw(no_plan);
use XML::LibXML;

## Create a parser object
my $parser; eval {$parser = XML::LibXML->new()};
ok(!$@, "created parser object (XML::LibXML works)");

## Check if parser chokes on empty strings
my $EMPTY_STR = "";
eval {$parser->parse_string($EMPTY_STR)};
ok($@,'parser chokes on empty strings');

