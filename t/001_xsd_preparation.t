use warnings;
use strict;

use Test::More qw(no_plan);

ok(opendir(my $xsd_dir_h, "./xsd/"),           'can access the xsd directory');
ok(my @xsd_dir = readdir $xsd_dir_h,           'can read xsd directory');

my @xsds = grep { /\.xsd$/ } @xsd_dir;
my $num_xsds = @xsds;

ok(scalar @xsds > 0,                           "found xsd files ($num_xsds)");

