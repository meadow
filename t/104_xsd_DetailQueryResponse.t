use warnings;
use strict;

use Test::More qw(no_plan);
use XML::LibXML;

require 't/lib/Common.pm';

my $xsd = "./xsd/DetailQueryResponse.xsd";
my $parser = XML::LibXML->new();

## Create a validator object
my $xs = create_xs($xsd);

#### XML tests
my $fn;                        # filename
my $test;                      # test description (english)
my $failure = 1; my $diag = 1; # true values

### Valid XML tests
### Invalid XML tests

